package com.mjparada.test.webservices;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mjparada.test.R;
import com.mjparada.test.core.zipHelper;
import com.mjparada.test.maps.WSMapsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class WebServicesActivity extends AppCompatActivity {
    Button buttonWSZIPConsume;
    ProgressDialog pdEjecucion;
    private static final int REQUEST_PERMIT_WRITE_READ = 1243;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_services);
        declareElements();
    }

    private void declareElements() {
        buttonWSZIPConsume = findViewById(R.id.buttonWSZIPConsume);
        buttonWSZIPConsume.setOnClickListener(buttonWSZIPConsumeOnClickListener);
    }

    private View.OnClickListener buttonWSZIPConsumeOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                String strWRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                String strREAD_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
                if ((checkSelfPermission(strWRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
                        (checkSelfPermission(strREAD_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(WebServicesActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMIT_WRITE_READ);
                } else {
                    consumirWS();
                }
            } else {
                consumirWS();
            }

        }
    };

    public void closepdEjecucion() {
        if (pdEjecucion.isShowing()) {
            pdEjecucion.dismiss();
        }
    }

    private void consumirWS() {
        pdEjecucion = ProgressDialog.show(WebServicesActivity.this, "Cargando Puntos ...", "Cargando Puntos ...", true);
        RequestQueue rqMain = Volley.newRequestQueue(this);
        StringRequest crearEvento = new StringRequest(Request.Method.POST, "http://74.205.41.248:8081/pruebawebservice/api/mob_sp_GetArchivo",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jaRespon = new JSONArray(response);
                            for (int i = 0; i < jaRespon.length(); i++) {
                                try {
                                    JSONObject object = jaRespon.getJSONObject(i);
                                    String strZipUrl = object.getString("valueResponse");
                                    new DownloadAsyncTask(WebServicesActivity.this, strZipUrl).execute();

                                } catch (JSONException e) {
                                    closepdEjecucion();
                                }
                            }

                        } catch (Exception e) {
                            closepdEjecucion();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    closepdEjecucion();
                } catch (Exception e) {
                }

            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parametros = new HashMap<String, String>();
                String id = "55";
                String user = "12984";
                parametros.put("serviceId", id);
                parametros.put("userId", user);
                return parametros;
            }
        };
        rqMain.add(crearEvento);
    }


    class DownloadAsyncTask extends AsyncTask<Void, Void, Void> {

        public DownloadAsyncTask(AppCompatActivity activit, String url) {
            this.activit = activit;
            this.strURL = url;

        }

        private AppCompatActivity activit;
        private String strURL;

        protected void onPreExecute() {
        }

        protected Void doInBackground(Void... params) {

            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            String p = path.getAbsolutePath();
            File dir = new File(p + "/ZIP/");
            try {
                dir.mkdirs();
            } catch (Exception e) {
                closepdEjecucion();
            }

            zipHelper zh = new zipHelper(activit);
            zh.downloadZip(strURL, p + "/ZIP/datos.zip");
            Intent intentMaps = new Intent(WebServicesActivity.this, WSMapsActivity.class);
            WebServicesActivity.this.startActivity(intentMaps);
            return null;
        }

        protected void onPostExecute(Void result) {
            closepdEjecucion();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMIT_WRITE_READ: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    consumirWS();
                } else {
                    Toast.makeText(this, "Acepte los permisos de la aplicacion", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        }
    }

}
