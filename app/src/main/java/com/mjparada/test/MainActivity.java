package com.mjparada.test;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mjparada.test.CRUD.CrudActivity;
import com.mjparada.test.Threads.ThreadActivity;
import com.mjparada.test.maps.MapsActivity;
import com.mjparada.test.webservices.WebServicesActivity;

public class MainActivity extends AppCompatActivity {
    Button buttonMap, buttonWebServices, buttonThread, buttonCRUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        declareElements();
        animateLinearL();
    }

    private void declareElements() {
        buttonMap = findViewById(R.id.buttonMap);
        buttonMap.setOnClickListener(buttonMapOnClickListener);
        buttonWebServices = findViewById(R.id.buttonWebServices);
        buttonWebServices.setOnClickListener(buttonWebServicesOnClickListener);
        buttonThread = findViewById(R.id.buttonThread);
        buttonThread.setOnClickListener(buttonThreadServicesOnClickListener);
        buttonCRUD = findViewById(R.id.buttonCrud);
        buttonCRUD.setOnClickListener(buttonCRUDOnClickListener);

    }

    private void animateLinearL() {
        LinearLayout your_Layout = (LinearLayout) findViewById(R.id.main_container);
        AnimationDrawable animationDrawable = (AnimationDrawable) your_Layout.getBackground();
        animationDrawable.setEnterFadeDuration(4000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();
    }

    private View.OnClickListener buttonMapOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intentMaps = new Intent(MainActivity.this, MapsActivity.class);
            MainActivity.this.startActivity(intentMaps);
        }
    };
    private View.OnClickListener buttonWebServicesOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intentMaps = new Intent(MainActivity.this, WebServicesActivity.class);
            MainActivity.this.startActivity(intentMaps);
        }
    };
    private View.OnClickListener buttonThreadServicesOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intentThread = new Intent(MainActivity.this, ThreadActivity.class);
            MainActivity.this.startActivity(intentThread);
        }
    };
    private View.OnClickListener buttonCRUDOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intentCrud = new Intent(MainActivity.this, CrudActivity.class);
            MainActivity.this.startActivity(intentCrud);
        }
    };
}
