package com.mjparada.test.core;


import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class zipHelper {
    private AppCompatActivity activity;

    public zipHelper(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void downloadZip(String strUrl, String destino) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(strUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            input = connection.getInputStream();
            new File(destino).createNewFile();

            output = new FileOutputStream(destino);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return;
        } finally {
            try {
                if (output != null) output.close();
                if (input != null) input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (connection != null) connection.disconnect();

        }
        File f = new File(destino);
        if (f.exists()) {
            checkFile(f);
            unZip(destino);
        }
    }

    public boolean unZip(String rutaArchivo) {
        InputStream fis;
        ZipInputStream zis;
        try {
            File archivoZip = new File(rutaArchivo);
            String FolderPadre = archivoZip.getParentFile().getPath();
            String nombreArchivo;

            fis = new FileInputStream(rutaArchivo);
            zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;
            while ((ze = zis.getNextEntry()) != null) {
                nombreArchivo = ze.getName();

                if (ze.isDirectory()) {
                    File fmd = new File(FolderPadre + "/" + nombreArchivo);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(FolderPadre + "/" + nombreArchivo);

                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }
            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void checkFile(File file) {
        MediaScannerConnection.scanFile(activity,
                new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
    }
}