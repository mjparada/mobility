package com.mjparada.test.CRUD.models;

public class ModelUser {

    private int id;
    private String birthdate;
    private String position;
    private String name;


    public ModelUser() {
    }
    public ModelUser(int id, String name, String birthdate, String position) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.position = position;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthdate;
    }

    public void setBirthDate(String birthdate) {
        this.birthdate = birthdate;
    }
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
