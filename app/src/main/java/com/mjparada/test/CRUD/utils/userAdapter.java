package com.mjparada.test.CRUD.utils;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mjparada.test.CRUD.models.ModelUser;

import com.mjparada.test.R;
import java.util.Date;
import java.util.List;
public class userAdapter extends RecyclerView.Adapter<userAdapter.MyViewHolder> {

    private Context context;
    private List<ModelUser> usersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textviewname;
        public TextView textviewbirthdate, textviewposition;

        public MyViewHolder(View view) {
            super(view);
            textviewname = view.findViewById(R.id.textviewname);
            textviewposition = view.findViewById(R.id.textviewposition);

            textviewbirthdate = view.findViewById(R.id.textviewbirthdate);
        }
    }
    public userAdapter(Context context, List<ModelUser> userAdapter) {
        this.context = context;
        this.usersList = userAdapter;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ModelUser u = usersList.get(position);
        holder.textviewname.setText(u.getName());
        holder.textviewbirthdate.setText((u.getBirthDate()));
        holder.textviewposition.setText(u.getPosition());

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }
    private String formatDate(String dateStr) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateStr);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM d");
            return fmtOut.format(date);
        } catch (ParseException e) {

        }

        return "";
    }
}