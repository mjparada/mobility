package com.mjparada.test.CRUD.db;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbUser extends SQLiteOpenHelper {

    public dbUser(Context context) {
        super(context, "dbTest", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlQueryCreateDB = "CREATE TABLE users ( "
                + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + " name TEXT,"
                + " position TEXT,"
                + " birthdate DATETIME "
                + ")";

        db.execSQL(sqlQueryCreateDB);
/*

        insertUsuario("Miguel Cervantes","08-Dic-1990","Desarrollador");
        insertUsuario("Juan Morales","14-Oct-1990","Desarrollador");
        insertUsuario("Roberto Méndes","14-Oct-1990","Desarrollador");
        insertUsuario("Miguel Cuevas","08-Dic-1990","Desarrollador");*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS  users" );
        onCreate(db);
    }

    public long insertUsuario(String strName,String strBdate, String strPosition) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues vContentValues = new ContentValues();
        vContentValues.put("name", strName);
        vContentValues.put("birthdate", strBdate);
        vContentValues.put("position", strPosition);
        long id = db.insert("users", null, vContentValues);
        db.close();
        return id;
    }

    public Cursor getUser(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users where id = '"+ Long.toString(id)  +"'",null);

        return cursor;
    }

    public Cursor getAllUsers() {
        String selectQuery = "SELECT  * FROM users ORDER BY id DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public int getUsersCount() {
        String countQuery = "SELECT  * FROM users" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public int updateUser(long id, String strName,String strBdate, String strPosition) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues vContentValues = new ContentValues();
        vContentValues.put("name",strName);
        vContentValues.put("birthdate",strBdate);
        vContentValues.put("position", strPosition);


        return db.update("users", vContentValues,  "id = ?",
                new String[]{String.valueOf(id)});
    }

    public void deleteUser(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("users",  "id = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }
}
