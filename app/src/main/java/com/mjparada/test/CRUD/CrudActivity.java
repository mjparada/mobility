package com.mjparada.test.CRUD;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mjparada.test.CRUD.db.dbUser;
import com.mjparada.test.CRUD.models.ModelUser;
import com.mjparada.test.CRUD.utils.UserDecoration;
import com.mjparada.test.CRUD.utils.userAdapter;
import com.mjparada.test.CRUD.utils.RecyclerTouchListener;
import com.mjparada.test.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CrudActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private userAdapter mAdapter;
    private List<ModelUser> usersList = new ArrayList<>();
    private dbUser db;
    EditText edittextbirthdate;
    Button buttonAdd;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud);
        declareElements();
        declareEventsElements();
    }

    private void database_init() {
        db = new dbUser(this);
        insert_init();
        usersList.addAll(getall(db.getAllUsers()));
    }

    private void declareElements() {
        recyclerView = findViewById(R.id.recycler_view);
        buttonAdd = findViewById(R.id.buttonAdd);
        database_init();
        mAdapter = new userAdapter(this, usersList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new UserDecoration(this, LinearLayoutManager.VERTICAL, 15));
        recyclerView.setAdapter(mAdapter);
    }

    private void declareEventsElements() {
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(false, -1, null);
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                showOpciones(position);
            }
        }));

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                edittextbirthdate.setText(date);
            }
        };
    }

    public List<ModelUser> getall(Cursor cursor) {
        List<ModelUser> usuarios = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                ModelUser usuario = new ModelUser();
                usuario.setId(cursor.getInt(cursor.getColumnIndex("id")));
                usuario.setName(cursor.getString(cursor.getColumnIndex("name")));
                usuario.setBirthDate(cursor.getString(cursor.getColumnIndex("birthdate")));
                usuario.setPosition(cursor.getString(cursor.getColumnIndex("position")));
                usuarios.add(usuario);
            } while (cursor.moveToNext());
        }

        db.close();

        return usuarios;
    }

    private void insert_init() {
        if (db.getUsersCount() == 0) {
            db.insertUsuario("Miguel Cervantes", "08/12/1990", "Desarrollador");
            db.insertUsuario("Juan Morales", "14/08/1990", "Desarrollador");
            db.insertUsuario("Roberto Méndes", "14/08/1990", "Desarrollador");
            db.insertUsuario("Miguel Cuevas", "08/12/1990", "Desarrollador");

        }
    }
    private void showOpciones(final int position) {
        CharSequence colors[] = new CharSequence[]{"Editar", "Borrar"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("seleciona opcion");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showDialog(true, position, usersList.get(position));
                } else {

                    db.deleteUser(usersList.get(position).getId());

                    usersList.remove(position);
                    mAdapter.notifyItemRemoved(position);

                }
            }
        });
        builder.show();
    }

    private void showDialog(final boolean update, final int position, final ModelUser u) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.dialog_crud, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(CrudActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText edittextname = view.findViewById(R.id.edittextname);
        edittextbirthdate = view.findViewById(R.id.edittextbirthdate);

        edittextbirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        edittextbirthdate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    showDatePickerDialog();
                } else {

                }
            }
        });
        final EditText edittextposition = view.findViewById(R.id.edittextposition);
        TextView textview_title = view.findViewById(R.id.textview_title);
        textview_title.setText(!update ? "Nuevo Usuario" : "Editar Usuario");

        if (update) {
            edittextname.setText(u.getName());
            edittextbirthdate.setText(u.getBirthDate());
            edittextposition.setText(u.getPosition());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(update ? "actualizar" : "guardar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edittextname.getText().toString().trim().equals("")) {
                    Toast.makeText(CrudActivity.this, "coloca nombre!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edittextbirthdate.getText().toString().trim().equals("")) {
                    Toast.makeText(CrudActivity.this, "coloca fecha!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edittextposition.getText().toString().trim().equals("")) {
                    Toast.makeText(CrudActivity.this, "coloca puesto !", Toast.LENGTH_SHORT).show();
                    return;
                }
                alertDialog.dismiss();


                if (update) {
                    ModelUser u = usersList.get(position);
                    u.setName(edittextname.getText().toString());
                    u.setBirthDate(edittextbirthdate.getText().toString());
                    u.setPosition(edittextposition.getText().toString());

                    db.updateUser(position, edittextname.getText().toString(), edittextbirthdate.getText().toString(), edittextposition.getText().toString());
                    usersList.set(position, u);
                    mAdapter.notifyItemChanged(position);
                } else {
                    long id = db.insertUsuario(edittextname.getText().toString(), edittextbirthdate.getText().toString(), edittextposition.getText().toString());


                    Cursor uCurso = db.getUser(id);

                    if (uCurso != null) {
                        uCurso.moveToFirst();
                        ModelUser u = new ModelUser(
                                uCurso.getInt(uCurso.getColumnIndex("id")),
                                uCurso.getString(uCurso.getColumnIndex("name")),
                                uCurso.getString(uCurso.getColumnIndex("birthdate")),
                                uCurso.getString(uCurso.getColumnIndex("position"))
                        );

                        uCurso.close();
                        if (u != null) {
                            usersList.add(0, u);
                            mAdapter.notifyDataSetChanged();

                        }
                    }


                }
            }
        });
    }

    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                CrudActivity.this,
                CrudActivity.this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "/" + month + "/" + year;
        edittextbirthdate.setText(date);
    }
}
