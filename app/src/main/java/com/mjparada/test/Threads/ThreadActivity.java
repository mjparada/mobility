package com.mjparada.test.Threads;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.mjparada.test.R;
public class ThreadActivity extends AppCompatActivity implements messages {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        createElements();
    }

    private void createElements() {
        LinearLayout linearlayoutContenedor;
        Button buttonThread;
        buttonThread = new Button(this);
        buttonThread.setText(R.string.iniciaHilo);
        linearlayoutContenedor = findViewById(R.id.linearlayoutContenedor);
        LinearLayout.LayoutParams LinearLayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        linearlayoutContenedor.addView(buttonThread, LinearLayoutParameters);
        buttonThread.setOnClickListener(buttonThreadOnClickListener);
        buttonThread.setBackgroundResource(R.color.colorPrimary);

    }


    private View.OnClickListener buttonThreadOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final int mstime = 10000;
            Thread bg = new Thread(new Runnable() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    Thread.sleep(mstime);


                                    show();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } finally {
                                }
                            }
                        });
                    } catch (Throwable t) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                            }
                        });

                    }
                }
            });
            bg.start();
        }
    };

    public void show() {
        Toast.makeText(getApplicationContext(), R.string.tiempo_completado, Toast.LENGTH_LONG).show();
    }
}

