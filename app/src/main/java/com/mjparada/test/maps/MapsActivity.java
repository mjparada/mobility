package com.mjparada.test.maps;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mjparada.test.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private SupportMapFragment mf;
    private GoogleMap mMap;
    Button buttonBuscar;
    EditText edittextCoordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        declareElements();
    }

    private void declareElements() {
        mf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mf.getMapAsync(this);
        buttonBuscar = findViewById(R.id.buttonBuscar);
        edittextCoordinates = findViewById(R.id.edittextCoordinates);
        buttonBuscar.setOnClickListener(buscarOnClickListener);
    }

    private View.OnClickListener buscarOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            mMap.clear();
            String strCoordenates = edittextCoordinates.getText().toString().trim();
            if (strCoordenates.equals("")) {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.insertCoordenates, Toast.LENGTH_LONG);

                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
                edittextCoordinates.setText("");

                return;
            }
            if (strCoordenates.indexOf(",") == -1) {
                Toast.makeText(getApplicationContext(), R.string.formatoincorrecto, Toast.LENGTH_LONG).show();
                edittextCoordinates.setText("");
                return;
            }
            String[] arrayC = strCoordenates.split("\\,", -1);
            if (arrayC.length != 2) {
                Toast.makeText(getApplicationContext(), R.string.formatoincorrecto, Toast.LENGTH_LONG).show();
                edittextCoordinates.setText("");
                return;
            }

            if (!onlyNum(arrayC[0].trim())) {
                Toast.makeText(getApplicationContext(), R.string.formatoincorrecto, Toast.LENGTH_LONG).show();
                edittextCoordinates.setText("");
                return;
            }
            if (!onlyNum(arrayC[1].trim())) {
                Toast.makeText(getApplicationContext(), R.string.formatoincorrecto, Toast.LENGTH_LONG).show();
                edittextCoordinates.setText("");
                return;
            }
            LatLng latLng = new LatLng(Double.parseDouble(arrayC[0].trim()), Double.parseDouble(arrayC[1].trim()));
            mMap.addMarker(new MarkerOptions().position(latLng).title(""));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private static boolean onlyNum(String strNum) {
        try {
            double dbl = Double.parseDouble(strNum);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


}
