package com.mjparada.test.maps;

import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mjparada.test.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class WSMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private SupportMapFragment mf;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_ws);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        declareElements();
    }

    private void declareElements() {
        mf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mf.getMapAsync(this);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String strAp = path.getAbsolutePath() + "/ZIP/";
        String filename = "cargaInicial_2019-03-15_12984.txt";
        File f = new File(strAp + filename);
        if (f.exists()) {
            loadFile(strAp, filename);
        }
    }


    private void loadFile(String strAp, String fileName) {
        try {
            String Resultado = readTextFile(strAp, fileName);
            JSONObject search = new JSONObject(Resultado);

            JSONArray ja = new JSONArray(search.getJSONArray("data").getJSONObject(0).getJSONArray("UBICACIONES").toString());
            JSONArray jsonArrayResult = new JSONArray();
            for (int t = 0; t < ja.length(); t++) {
                try {

                    JSONObject ob = ja.getJSONObject(t);
                    String strLatitud = Double.toString(ob.getDouble("FNLATITUD"));
                    String strLongitud = Double.toString(ob.getDouble("FNLONGITUD"));
                    String strTitulo = ob.getString("FCNOMBRE");

                    JSONObject joCoordenadas = new JSONObject();
                    try {
                        joCoordenadas.put("la", strLatitud);
                        joCoordenadas.put("lo", strLongitud);
                        joCoordenadas.put("title", strTitulo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    jsonArrayResult.put(joCoordenadas);

                } catch (JSONException e) {
                    Log.e("error", "Json error: " + e.getMessage());
                }

            }

            jsonToMap(jsonArrayResult.toString());

        } catch (JSONException e) {
            Log.e("error", "Json error: " + e.getMessage());
        }

    }

    private void jsonToMap(String strArr) {

        if (!strArr.trim().equals("")) {
            try {
                JSONArray j = new JSONArray(strArr);
                Double la = 0.0;
                Double lo = 0.0;
                String t = "";
                for (int i = 0; i < j.length(); i++) {
                    try {
                        JSONObject object = j.getJSONObject(i);
                        la = object.getDouble("la");
                        lo = object.getDouble("lo");
                        t = object.getString("title");
                        LatLng newP = new LatLng(la, lo);
                        if (t == null) {
                            t = "";
                        }
                        mMap.addMarker(new MarkerOptions().position(newP).title(t));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                LatLng last = new LatLng(la, lo);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(last));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mMap.clear();

        }

    }

    public String readTextFile(String strPath, String strName){
        File file = new File(strPath,strName);
        StringBuilder txt = new StringBuilder();
        try {
            BufferedReader buffr = new BufferedReader(new FileReader(file));
            String line;

            while ((line = buffr.readLine()) != null) {
                txt.append(line);
                txt.append('\n');
            }
            buffr.close();
        }
        catch (IOException e) {
        }
        return txt.toString();
    }
}
